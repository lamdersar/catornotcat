set :rails_env, "production"

set :application, "catornotcat"
set :repository,  "git@bitbucket.org:tommyfun/catornotcat.git"
set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :branch, "production"

set :user, "deploy"
set :deploy_to, "/home/deploy/catornotcat"
set :use_sudo, false
set :rake, "/usr/local/rvm/gems/ruby-1.9.3-p194@global/bin/rake"

role :web, "ec2-184-73-40-56.compute-1.amazonaws.com"      # Your HTTP server, Apache/etc
role :app, "ec2-184-73-40-56.compute-1.amazonaws.com"      # This may be the same as your `Web` server
role :db,  "ec2-184-73-40-56.compute-1.amazonaws.com", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

set :normalize_asset_timestamps, false

set :app_server, :passenger

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
 task :start do ; end
 task :stop do ; end
 task :restart, :roles => :app, :except => { :no_release => true } do
   run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
 end
end