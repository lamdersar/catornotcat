class CreateCats < ActiveRecord::Migration
  def change
    create_table :cats do |t|
      t.attachment :picture
      t.integer :yes
      t.integer :no

      t.timestamps
    end
  end
end
